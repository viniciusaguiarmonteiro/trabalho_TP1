/** file: springmass.cpp
 ** brief: SpringMass simulation implementation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"
#include <iostream>

/* ---------------------------------------------------------------- */
// class Mass
/* ---------------------------------------------------------------- */

Mass::Mass()
: position(), velocity(), force(), mass(1), radius(1),
xmin(-1),xmax(1),ymin(-1),ymax(1) 
{ }

Mass::Mass(Vector2 position, Vector2 velocity, double mass, double radius)
: position(position), velocity(velocity), force(), mass(mass), radius(radius),
xmin(-1),xmax(1),ymin(-1),ymax(1)
{ }

void Mass::setForce(Vector2 f)
{
  force = f ;
}

void Mass::addForce(Vector2 f)
{
  force = force + f ;
}

Vector2 Mass::getForce() const
{
  return force ;
}

Vector2 Mass::getPosition() const
{
  return position ;
}

Vector2 Mass::getVelocity() const
{
  return velocity ;
}

double Mass::getRadius() const
{
  return radius ;
}

double Mass::getMass() const
{
  return mass ;
}

double Mass::getEnergy(double gravity) const
{
  //---------TASK 11-----------//

  double energy = 0 ;

  double ec = (this->getMass()) * (this->getVelocity().norm2());
  ec *= 0.5;

  double epg = (this->getMass()) * gravity * (this->getPosition().y - this->getRadius());

  energy = ec + epg;

  return energy ;
}

void Mass::step(double dt)
{
  //---------TASK 11-----------//
  
  Vector2 a = (this->getForce()) / (this->getMass());
  
  position.x = position.x + (velocity.x*dt) + (a.x * (dt*dt))/2; //atualizando posicao eixo x
  if(position.x >= xmax)
  {
  	position.x = xmax - radius;
  }
  if(position.x <= xmin)
  {
  	position.x = xmin + radius;
  }

  position.y = position.y + (velocity.y*dt) + (a.y * (dt*dt))/2; //atualizando posicao eixo y
  if(position.y >= ymax)
  {
  	position.y = ymax - radius;
  }
  if(position.y <= ymin)
  {
  	position.y = ymin + radius;
  }
  
  velocity = velocity + (a*dt); //atuzalicando velocidade nos eixos
  
  if (position.x == xmax - radius || position.x == xmin + radius) {
    velocity.x = -velocity.x ;
  }
  if (position.y == ymax - radius || position.y == ymin + radius) {
    velocity.y = -velocity.y ;
  }
}

/* ---------------------------------------------------------------- */
// class Spring
/* ---------------------------------------------------------------- */

Spring::Spring(Mass * mass1, Mass * mass2, double naturalLength, double stiffness, double damping)
: mass1(mass1), mass2(mass2),
naturalLength(naturalLength), stiffness(stiffness), damping(damping)
{ }

Mass * Spring::getMass1() const
{
  return mass1 ;
}

Mass * Spring::getMass2() const
{
  return mass2 ;
}

Vector2 Spring::getForce() const
{
  //---------TASK 12-------------
  
  Vector2 F ;
  
  Vector2 d = (mass2->getPosition()) - (mass1->getPosition()); //distancia entre as duas massas
  
  double l = this->getLength();
  
  d = d/l; //vetor unitario da direcao da mola
  
  Vector2 V = (mass2->getVelocity()) - (mass1->getVelocity()); //Vetor da velocidade de alongamento
  
  double VA = dot(V, d); //Velocidade de Alongamento
  
  double aux = (naturalLength - l) * stiffness - (VA * damping); //modulo da forca
  
  F = aux * d; //Vetor forca
  
  return F ;
}

double Spring::getLength() const
{
  Vector2 u = mass2->getPosition() - mass1->getPosition() ;
  return u.norm() ;
}

double Spring::getEnergy() const {
  double length = getLength() ;
  double dl = length - naturalLength;
  return 0.5 * stiffness * dl * dl ;
}

std::ostream& operator << (std::ostream& os, const Mass& m)
{
  os << m.getPosition().x << " " << m.getPosition().y << " ";
  return os ;
}

std::ostream& operator << (std::ostream& os, const Spring& s)
{
  return os<<"$"<<s.getLength() ;
}

/* ---------------------------------------------------------------- */
// class SpringMass : public Simulation
/* ---------------------------------------------------------------- */

SpringMass::SpringMass(double gravity)
: gravity(EARTH_GRAVITY)
{ }

void SpringMass::display()
{
  Molas::iterator it;
  Massas::iterator jt;
  
  for(jt = vetMass.begin(); jt != (vetMass.end()); jt++)
  {
  	std::cout << *jt ;
  }
  
}

double SpringMass::getEnergy()
{
  double energy = 0 ;
  
  Molas::iterator it;
  Massas::iterator jt;
  
  for(jt = vetMass.begin(); jt != (vetMass.end()); jt++)
  {
  	energy += jt->getEnergy(EARTH_GRAVITY);
  }
  
  for(it = vetSpring.begin(); it != (vetSpring.end()); it++)
  {
  	energy += it->getEnergy();
  }

  return energy ;
}

void SpringMass::step(double dt)
{
  Vector2 g(0,-1 * EARTH_GRAVITY) ; // Vetor da gravidade, i.e.,
  // aceleracao apontando para baixo.
  
  Massas::iterator it = vetMass.begin();
  Molas::iterator jt = vetSpring.begin();
  
  while (it != vetMass.end())
  {
  	it->setForce(it->getMass() * g);
  	it++;
  }
  
  while (jt != vetSpring.end())
  {
  	jt->getMass1()->addForce(-1 * (jt->getForce()));
  	jt->getMass2()->addForce(+1 * (jt->getForce()));
  	jt++;
  }
  
  it = vetMass.begin();
  while (it != vetMass.end())
  {
  	it->step(dt);
  	it++;
  }
}
 
 //-----------TASK 13-------------
void SpringMass::addMass(Mass *m){

	vetMass.push_back(*m);
}

void SpringMass::addSpring(int i, int j, double naturalLength, double stiffness, double damping){

	Spring s(&vetMass.at(i), &vetMass.at(j), naturalLength, stiffness, damping);
	vetSpring.push_back(s); //Adicionando a mola ao vetor de molas
}
