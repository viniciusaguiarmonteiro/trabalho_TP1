var indexSectionsWithContent =
{
  0: "abdefghmnoprstuvxy~",
  1: "bdfmsv",
  2: "bgrst",
  3: "abdfghmnorsuv~",
  4: "dfgmnprsvxy",
  5: "fm",
  6: "em",
  7: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Pages"
};

