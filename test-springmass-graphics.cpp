/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{
  private:
  	Figure figure ;

  public:
	SpringMassDrawable()
	: figure("SpringMass Graphics")
	{
		figure.addDrawable(this) ;
	}

	void draw() {
		  	double thickness=1.5;

		  	for(int i=0; i < vetSpring.size(); i++){
		  		figure.drawCircle(vetSpring.at(i).getMass1()->getPosition().x, vetSpring.at(i).getMass1()->getPosition().y, vetSpring.at(i).getMass1()->getRadius()) ;
		  		figure.drawCircle(vetSpring.at(i).getMass2()->getPosition().x, vetSpring.at(i).getMass2()->getPosition().y, vetSpring.at(i).getMass2()->getRadius()) ;
		  		figure.drawLine(vetSpring.at(i).getMass1()->getPosition().x, vetSpring.at(i).getMass1()->getPosition().y, vetSpring.at(i).getMass2()->getPosition().x, vetSpring.at(i).getMass2()->getPosition().y, thickness);
		  	}
	}

	void display() {
	  figure.update() ;
	}
};

int main(int argc, char** argv){

  glutInit(&argc,argv) ;

  SpringMassDrawable springmass ;

  const double mass = 0.05 ;
  const double radius = 0.09 ;
  const double naturalLength = 0.95 ;
  const double stiffness = 0.55;
  const double damping = 0.01;

  Mass m1(Vector2(0.3,0.9), Vector2(2,0), mass, radius) ;
  Mass m2(Vector2(-0.3,0.9), Vector2(2,0), 0.1, radius) ;

  springmass.addMass(&m1);
  springmass.addMass(&m2);
  springmass.addSpring(0, 1, naturalLength, stiffness, damping);


  run(&springmass, 1/120.0) ;


  return 0;
}
