# MANUAL DE INSTRUÇÕES - BOUNCING BALL
## OBJETIVO

O objetivo desse projeto é gerar o caminho (coordenadas) que a bola irá percorrer dentro dos eixos (x,y) do plano cartesiano, salvar em um arquivo texto e gerar o gráfico referente a posição da bola com as 10.000 coordenadas geradas pelo programa.

## REQUISITOS

Sistema operacional: <strong>Microsoft Windows 10</strong><br>
CMD: <strong>Microsoft Windows</strong><br>
CMD version: <strong>[versão 10.0.15063]</strong><br>
Bash: <strong>GNU bash</strong><br>
Bash version: <strong>4.4.12(1)-release (x86_64-pc-msys)</strong><br>
g++: <strong>g++ (tdm64-1)</strong><br>
G++ version: <strong>5.1.0</strong><br>
Bibliotecas:`#include <iostream>`<br>
IDE de desenvolvimento: <strong>Code::Blocks 16.01</strong><br>
Gerar gráfico: <strong>Microsoft Excel 2016</strong><br>

## ARQUIVOS

<strong>[ball.h](ball.h):</strong> Arquivo que contém Classe "Ball" e suas assinaturas de funções e atributos<br>
<strong>[ball.cpp](ball.cpp):</strong> Arquivo que contém a implementação das funções e construtor do objeto "Ball"<br>
<strong>[test-ball.cpp](test-ball.cpp):</strong> Arquivo que contém a "main", onde são chamadas as funções<br>

## DIAGRAMA DE CLASSES

![gráfico](/img/DIAGRAMA01.png)

## PASSOS A SEGUIR

### GERAR O EXECUTÁVEL
Para compilar o programa e gerar as coordenadas é necessário entrar no diretório (caminho do projeto) e executar o comando abaixo pelo Bash Windows(CMD):<br>
```
g++ ball.h ball.cpp test-ball.cpp
```
OBS.: Por padrão ele gera o executável com o nome `a.exe`<br>

### GERAR ARQUIVO TEXTO
Para salvar a saída do programa em um arquivo texto é necessário executar o comando abaixo no Bash do Windows(CMD):<br>
```
a > teste1.txt
```
Arquivo com a saida do programa:
[ArquivoCoordenadas(x,y).txt](/teste1.txt)

### GERAR GRÁFICO
Para gerar o gráfico com as coordenadas da bola é necessário exportar as coordenadas do arquivo texto criado para o Microsoft Excel 2016.
Selecione a coluna e aperte "ctrl + u" e em "Localizar" digite "." e em "substituir" digite ",", pois assim o software irá reconhecer como número<br>
Após importar as coordenadas ele irá colocar as duas coordenadas em uma única coluna, sendo necessário separar as duas coordenadas em duas colunas.<br>

Para separar em colunas:<br>
Selecione a coluna  "A" -> Dados -> Texto para Colunas -> "Delimitado" -> Selecionar o delimitador "espaços" -> Concluir<br>

Para inserir o gráfico:
Selecione as colunas "A" e "B" -> clique em "Inserir" -> Selecione "Inserir gráfico de dispersão (x,y)" -> selecione "Dispersão com linhas suaves"<br>

Para melhor visualização:<br>
Eixo vertical: Selecione o eixo vertical -> Selecione "Opções do eixo" -> Selecione "Rótulos" -> Selecione "Posição do Rótulo" -> Selecione "Inferior"<br>
Eixo horizontal: Selecione o eixo horizontal -> Selecione "Opções do eixo" -> Selecione "Rótulos" -> Selecione "Posição do Rótulo" -> Selecione "Inferior"<br>

![gráfico](/img/GRAFICO BOLA.png)

# MANUAL DE INSTRUÇÕES - SPRING-MASS

## OBJETIVO

O objetivo desse projteseto é gerar gerar as tragetória das duas bolas ligadas por uma mola, onde solta a partir de um ponto elas ficam pingando até perder a energia ficar em repouso.

## REQUISITOS

Sistema operacional: <strong>Ubuntu 14.04 LTS</strong><br>
Bash: <strong>GNU bash</strong><br>
Bash version: <strong>4.3.48(1)-release (x86_64-pc-linux-gnu)</strong><br>
g++: <strong>(Ubuntu 5.4.0-6ubuntu1~16.04.5)</strong><br>
G++ version: <strong>5.4.0 20160609</strong><br>
Editor de texto: <strong>Sublime Text Build 3152</strong><br>
Gerar gráfico: <strong>Microsoft Excel 2016</strong><br>
Bibliotecas:<br>
`#include <iostream>`<br>
`#include <vector>`<br>

## ARQUIVOS

<strong>[simulation.h](simulation.h):</strong> Protótipo das funções virtuais que serão implementadas nas classes filhas<br>
<strong>[test-springmass.cpp](test-springmass.cpp):</strong> Arquivo onde será implementada a função main, que fornecerá a saída dos dados referentes ao projeto<br>
<strong>[springmass.cpp](springmass.cpp):</strong> Arquivo que Implementa as funções dos arquivos springmass.h e simulation.h<br>
<strong>[springmass.h](springmass.h):</strong> Arquivo com os atributos e protótipos das funções que serão implementadas no arquivo springmass.cpp<br>

## DIAGRAMA DE CLASSES

![gráfico](/img/DIAGRAMA02.png)

## GERANDO AS COORDENADAS

Para gerar a saida com as coordenadas das duas bolas é necessário executar a linha de comando no bash do linux:
```
make test-springmass
```
Obs.: A linha de comando está armazenada em um arquivo makefile que contêm a linha  "springmass.cpp test-springmass.cpp" e executará o programa de forma mais fácil.

Após isso será necessário armazenar as coordenadas em arquivo texto com o comando abaixo:
```
./test-springmass > teste2.txt
```
Arquivo com a saida do programa:
[ArquivoCoordenadas2bolas(x,y).txt](/teste2.txt)

## GERAR GRÁFICO

Para gerar o gráfico com as coordenadas da bola é necessário exportar as coordenadas do arquivo texto criado para o Microsoft Excel 2016.<br>
Selecione a coluna e aperte "ctrl + u" e em "Localizar" digite "." e em "substituir" digite ",", pois assim o software irá reconhecer como número<br>
Após importar as coordenadas ele irá colocar as QUATRO coordenadas em uma única coluna, sendo necessário separar as quatro coordenadas em colunas.<br>

Para separar em colunas:<br>
Selecione a coluna  "A" -> Dados -> Texto para Colunas -> "Delimitado" -> Selecionar o delimitador "espaços" -> Concluir<br>

Para inserir o gráfico da Massa A:
Selecione as colunas "A" e "B" -> clique em "Inserir" -> Selecione "Inserir gráfico de dispersão (x,y)" -> selecione "Dispersão com linhas suaves"<br>

Para inserir o gráfico da Massa B:
Selecione as colunas "C" e "D" -> clique em "Inserir" -> Selecione "Inserir gráfico de dispersão (x,y)" -> selecione "Dispersão com linhas suaves"<br>

Para melhor visualização:<br>
Eixo vertical: Selecione o eixo vertical -> Selecione "Opções do eixo" -> Limites coloque o mínimo -1 e máximo 1 -> Em "eixo vertical cruza" selecione a última opção<br>
Eixo horizontal: Selecione o eixo horizontal -> Selecione "Opções do eixo" -> Limites coloque o mínimo -1 e máximo 1 -> Em "eixo horizontal cruza" selecione a última opção<br>
Troque a cor de um dos gráficos em selecione a linha -> selecione "linha sólida" -> em "cor", mude a cor na paleta de cores.
Repita esse processo para os dois gráficos
selecione um dos gráficos e em "preenchimento" coloque a opção de "sem preenchimento"
Por fim, sobreponha os gráficos.

![gráfico](/img/GRAFICO DUAS BOLAS.png)

# MANUAL DE INSTRUÇÕES - GRAPHICS

## OBJETIVO

O projeto tem a função de monstrar a trajetória representada gráficamentente dos conjuntos de coordenadas das massas das bolas ligadas por uma mola. As massas serão soltas e ficarão pingando até que perca toda a energia e entre em repouso<br>

## REQUISITOS

Sistema operacional: <strong>Ubuntu 14.04 LTS</strong><br>
Bash: <strong>GNU bash</strong><br>
Bash version: <strong>4.3.48(1)-release (x86_64-pc-linux-gnu)</strong><br>
g++: <strong>(Ubuntu 5.4.0-6ubuntu1~16.04.5)</strong><br>
g++ version: <strong>5.4.0 20160609</strong><br>
OpenGL version:<strong>3.0 Mesa 17.0.7</strong><br>
GLUT API version:<strong> 3.0 </strong>
command line de instalação: 
```sudo apt-get install freeglut3 freeglut3-dev```
Editor de texto: <strong>Sublime Text Build 3152</strong><br>
Gerar gráfico: <strong>Microsoft Excel 2016</strong><br>

Bibliotecas:<br>
```#include < iostream >```<br>
```#include < sstream >```<br>
```#include < iomanip >```<br>
```#include < cmath >```<br>
```#include < vector >```<br>

## ARQUIVOS

<strong>[graphics.h](graphics.h):</strong> Arquivo com os atributos e protótipos das funções que serão implementadas no arquivo graphics.cpp <br>
<strong>[test-springmass.cpp](test-springmass.cpp):</strong> Arquivo onde será implementada a função main, que fornecerá a saída dos dados referentes ao projeto<br>
<strong>[springmass.cpp](springmass.cpp):</strong> Arquivo que Implementa as funções dos arquivos springmass.h e simulation.h<br>
<strong>[springmass.h](springmass.h):</strong> Arquivo com os atributos e protótipos das funções que serão implementadas no arquivo springmass.cpp<br>
<strong>[test-springmass-graphics.cpp](test-springmass-graphics.cpp):</strong> Arquivo onde será implementada a função main, que fornecerá a saída gráfica do projeto SpringMass<br>
<strong>[test-ball-graphics.cpp](test-ball-graphics.cpp):</strong> Arquivo onde será implementada a função main, que fornecerá a saída gráfica do projeto Bouncing Ball<br>

## DIAGRAMA DE CLASSES

![gráfico](/img/DIAGRAMA02.png)

## PROGRAMA EM EXECUÇÃO

Para executar o programa é necessário abrir o bash do ubuntu e digitar o código abaixo:<br>
```make test-springmass-graphics```
Obs.: A linha de comando está armazenada em um arquivo makefile que contêm a linha "graphics.cpp springmass.cpp test-springmass-graphics.cpp"
e também os atributos de compilação do gráfico, assim executando o programa de forma mais fácil. 
Depois execute a linha de comando abaixo:<br>
``./test-springmass-graphics``

### TELA DE FUNCIONAMENTO
![gráfico](/img/TELA FUNCIONAMENTO.png)